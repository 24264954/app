<?php 
$I = new FunctionalTester($scenario);
$I->wantTo('perform actions and see result');

//When
$I->amOnPage('/');

//then
$I->seeCurrentURLEquals('/');
$I->see('Welcome to the Questionnaire Creator', '.title');
// And
$I->click('Login');

$I->click('Login');
// Then
$I->fillField('email', 'kel@email.com');
$I->fillField('password', 'password');
$I->click('Login');

$I->amOnPage('/home');
//And
$I->see('You are logged in');
$I->click('My Questionnaires');
// Then
$I->amOnPage('/admin/questionnaire/questionnaires');
// And
$I->see('My Questionnaires', 'h1');