<?php
  $I = new FunctionalTester($scenario);

  $I->am('user');
  $I->wantTo('add a question to an existing questionnaire');

  // Add database test data

  Auth::loginUsingId(1);

  // add a test user
  $I->haveRecord('users', [
      'id' => '40',
      'name' => 'testuser1',
      'email' => 'test1@user.com',
      'password' => 'password',
  ]);


  // add a test questionnaire to check that content can be seen in list at start

  $I->haveRecord('questionnaires', [
      'id' => '2222',
      'title' => 'Questionnaire 1',
      'description' => 'Questionnaire 1 description',
      'ethical_statement' => 'Questionnaire 1 ethical statement',
      'active' => 1,
      'user_id' => '40',
      ]);


  // test //
  // create a question
  // When
  $I->amOnPage("/admin/questionnaire/questionnaires");
  $I->see('My Questionnaires', 'h1');
  $I->see('Questionnaire 1');
  $I->click('Add Question');

  // Then
  $I->amOnPage('/admin/question/create');
  // And
  $I->see('Create Question', 'h1');

  $I->submitForm('#createquestion', [
      'question' => 'Question 1',]);
  // Then
  // create answers
  $I->amOnPage('/admin/answer/create');
  $I->submitForm('#createanswers', [
    'answer1' => 'Answer 1',
    'answer2' => 'Answer 2',
    'answer3' => 'Answer 3',
    ]);

  // Then
  $I->amOnPage('/admin/questionnaire/questionnaires');
  $I->see('Questionnaire 1');