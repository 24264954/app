<?php 
$I = new FunctionalTester($scenario);

$I->am('an administrator');
$I->wantTo('test Laravel is working');

//When
$I->amOnPage('/');

//then
$I->seeCurrentURLEquals('/');
$I->see('Welcome to the Questionnaire creator', '.title');
