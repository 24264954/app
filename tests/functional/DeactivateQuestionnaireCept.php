<?php 
$I = new FunctionalTester($scenario);
$I->wantTo('deactivate questionnaire');


$I = new FunctionalTester($scenario);

  $I->am('administrator');
  $I->wantTo('create a new questionnaire');

  // Add database test data

  // log in as seeded user
  Auth::loginUsingId(1);


  // Add db test data

  // add a test user
  $I->haveRecord('users', [
    'id' => '40',
    'name' => 'testuser1',
    'email' => 'test1@user.com',
    'password' => 'password',
]);


  // add a test questionnaire to check that content can be seen in list at start

  $I->haveRecord('questionnaires', [
      'id' => '2222',
      'title' => 'Questionnaire 1',
      'description' => 'Questionnaire 1 description',
      'ethical_statement' => 'Questionnaire 1 ethical statement',
      'active' => 1,
      'user_id' => '40',
  ]);
