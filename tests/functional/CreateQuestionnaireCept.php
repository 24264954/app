<?php
  $I = new FunctionalTester($scenario);

  $I->am('administrator');
  $I->wantTo('create a new questionnaire');

  // Add database test data

  // log in as seeded user
  Auth::loginUsingId(1);


  // Add db test data

  // add a test user
  $I->haveRecord('users', [
    'id' => '40',
    'name' => 'testuser1',
    'email' => 'test1@user.com',
    'password' => 'password',
]);


  // add a test questionnaire to check that content can be seen in list at start

  $I->haveRecord('questionnaires', [
      'id' => '2222',
      'title' => 'Questionnaire 1',
      'description' => 'Questionnaire 1 description',
      'ethical_statement' => 'Questionnaire 1 ethical statement',
      'active' => 1,
      'user_id' => '40',
  ]);


  // test //
  // create a questionnaire
  // When
  $I->amOnPage('questionnaire/questionnaires');
  $I->see('My Questionnaires', 'h1');
  $I->see('Questionnaire 1');
  $I->dontSee('Questionnaire 2');
  // And
  $I->click('Create Questionnaire');

  // Then
  $I->amOnPage('/admin/questionnaire/create');
  // And
  $I->see('Create Questionnaire', 'h1');

  $I->submitForm('#createquestionnaire', [
    'title' => 'Questionnaire 2',
    'content' => 'Questionnaire 2 description',
    'ethical_statement' => 'Questionnaire 2 ethical statement',
    'active' => 1,
  ]);
  // Then
  $I->amOnPage('/admin/questionnaire/questionnaires');
  $I->see('Questionnaire 2');