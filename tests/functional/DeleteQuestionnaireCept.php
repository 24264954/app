<?php 
$I = new FunctionalTester($scenario);

$I->am('user');
$I->wantTo('delete a questionnaire');

// Add database test data

Auth::loginUsingId(1);

// add a test user
$I->haveRecord('users', [
    'id' => '40',
    'name' => 'testuser1',
    'email' => 'test1@user.com',
    'password' => 'password',
]);


// add a test questionnaire to check that content can be seen in list at start

$I->haveRecord('questionnaires', [
    'id' => '2222',
    'title' => 'Questionnaire 1',
    'description' => 'Questionnaire 1 description',
    'ethical_statement' => 'Questionnaire 1 ethical statement',
    'active' => 1,
    'user_id' => '40',
     ]);


//test//
// delete the questionnaire
// When
$I->amOnPage('/questionnaire/questionnaires');
$I->see('h1');
// Then
$I->click('Delete Questionnaire');

$I->see('Are you sure?');
// And
$I->click('Ok');

$I->amOnPage('/admin/questionnaire/questionnaires');
$I->see('My Questionnaires', 'h1');
// And
$I->dontSee('Questionnaire 1');




