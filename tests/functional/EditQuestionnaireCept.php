<?php
  $I = new FunctionalTester($scenario);

  $I->am('user');
  $I->wantTo('edit a questionnaire');

  // Add database test data

  Auth::loginUsingId(1);

  // add a test user
  $I->haveRecord('users', [
      'id' => '40',
      'name' => 'testuser1',
      'email' => 'test1@user.com',
      'password' => 'password',
  ]);


  // add a test questionnaire to check that content can be seen in list at start

  $I->haveRecord('questionnaires', [
      'id' => '2222',
      'title' => 'Questionnaire 1',
      'description' => 'Questionnaire 1 description',
      'ethical_statement' => 'Questionnaire 1 ethical statement',
      'active' => 1,
      'user_id' => '40',
      ]);

//test
$I->amOnPage('/admin/questionnaire/questionnaires');
$I->see('My Questionnaires', 'h1');
$I->see('Questionnaire 1');
// Then
$I->click('Edit Questionnaire');

$I->amOnPage('/admin/questionnaire/edit');
$I->see('Edit Questionnaire - Questionnaire 1', 'h1');
// Then
$I->fillField('title', 'Questionnaire changed');
// And
$I->click('Update Questionnaire');

// Then
$I->amOnPage('/admin/questionnaire/questionnaires');
$I->see('My Questionnaires', 'h1');
$I->see('Questionnaire changed');
