-- MySQL dump 10.13  Distrib 5.7.33, for Linux (x86_64)
--
-- Host: localhost    Database: app
-- ------------------------------------------------------
-- Server version	5.7.33-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `question_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (1,'answer1',NULL),(2,'answer2',NULL),(3,'answer3',NULL),(4,'Facere dolorum repellat animi veritatis est aut animi.',16),(5,'Ipsa aspernatur aut tempora voluptas.',23),(6,'Aut rem et quasi eaque et veniam.',11),(7,'Laborum deserunt molestiae quia sunt accusamus est.',30),(8,'Dolorem minima et aut saepe quis.',23),(9,'Voluptatem pariatur qui numquam occaecati qui consequatur.',18),(10,'Et veniam dolorem dolor omnis.',11),(11,'Dolorem quia et fugiat quas aliquam placeat non.',8),(12,'Optio aut est a nam officiis eligendi.',11),(13,'Consequatur quidem aut est odit est.',29),(14,'Explicabo rerum minus neque aut id id in.',1),(15,'Amet quae et consectetur consectetur eveniet doloremque.',6),(16,'Laborum maxime illum nobis.',25),(17,'Est atque excepturi commodi tempore quis consequatur.',28),(18,'Accusantium aut sit soluta alias.',4),(19,'Iusto autem nam doloribus eveniet.',22),(20,'Veritatis inventore possimus et.',22),(21,'Fuga minus debitis sapiente.',26),(22,'Itaque aut consectetur aspernatur voluptas officiis.',28),(23,'Voluptas et minus tempora.',23),(24,'Doloremque ea explicabo et est dicta.',15),(25,'Ut accusantium maiores atque consequatur quia cum nemo.',28),(26,'Non occaecati aut quisquam.',2),(27,'Qui deleniti ut et voluptatem molestiae eos nostrum.',10),(28,'Occaecati natus explicabo consectetur magni quis sit qui.',10),(29,'Voluptas laboriosam id corporis facilis voluptatem.',16),(30,'Fugit dolorem sit ut aliquid iusto et assumenda eum.',19),(31,'Voluptas molestias harum quis animi blanditiis odit.',16),(32,'Officiis quod facilis adipisci est.',4),(33,'Necessitatibus minima unde eos ea dolorum.',26);
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (6,'2009_05_13_003826_create_permissions_table',1),(7,'2013_04_22_201403_create_roles_table',1),(8,'2014_10_12_000000_create_users_table',1),(9,'2014_10_12_100000_create_password_resets_table',1),(10,'2014_10_13_214650_create_questionnaires_table',1),(11,'2014_11_13_004216_create_role_users_table',1),(12,'2014_12_13_004017_create_permission_roles_table',1),(13,'2019_08_19_000000_create_failed_jobs_table',1),(14,'2021_04_20_215724_create_questions_table',1),(15,'2021_04_20_221247_create_answers_table',1),(16,'2021_04_22_220725_create_results_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1000);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'admin','admin',NULL,NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaires`
--

DROP TABLE IF EXISTS `questionnaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaires` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ethical_statement` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_on` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1122 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaires`
--

LOCK TABLES `questionnaires` WRITE;
/*!40000 ALTER TABLE `questionnaires` DISABLE KEYS */;
INSERT INTO `questionnaires` VALUES (1111,'Questionnaire 1','This is a questionnaire','This is an ethical statement',NULL,NULL,NULL,1,1),(1112,'Iste nisi necessitatibus ad voluptas iste rem sint.','Et exercitationem nam vero vero.','Et repellat et quidem consectetur.','2021-05-13 00:04:03','2021-05-13 00:04:03',NULL,1,17),(1113,'Id ratione qui magni tempore tenetur.','Saepe consectetur consequuntur cumque sequi dolores et at.','Eos aut odio reiciendis quis nulla et.','2021-05-13 00:04:03','2021-05-13 00:04:03',NULL,1,3),(1114,'Doloribus cum ut perspiciatis quaerat et.','Praesentium quam nobis qui eligendi enim doloribus.','Doloribus voluptatem et est quae repellat.','2021-05-13 00:04:03','2021-05-13 00:04:03',NULL,1,22),(1115,'Esse quasi dolorem beatae architecto.','Sit natus soluta quia tempora.','Reiciendis id animi alias distinctio.','2021-05-13 00:04:03','2021-05-13 00:04:03',NULL,1,23),(1116,'Veritatis provident enim id ut sed.','Sed et recusandae explicabo et.','Laudantium quo et quis distinctio harum ad atque.','2021-05-13 00:04:03','2021-05-13 00:04:03',NULL,1,24),(1117,'Distinctio qui quia fuga quisquam.','Ab officiis aut mollitia ut.','Ut est eos sed vitae.','2021-05-13 00:04:03','2021-05-13 00:04:03',NULL,1,13),(1118,'Commodi quas qui vel odit quam.','Facere facere molestias occaecati ea deleniti consequuntur dolorum.','Qui labore ut necessitatibus.','2021-05-13 00:04:03','2021-05-13 00:04:03',NULL,1,7),(1119,'Ea saepe reiciendis accusantium magnam.','Vel velit quasi laborum.','Omnis voluptatem id maxime.','2021-05-13 00:04:03','2021-05-13 00:04:03',NULL,1,15),(1120,'Quia illum adipisci sed sed.','Ex eaque sed unde non.','Voluptas delectus assumenda aperiam delectus sunt atque fugit.','2021-05-13 00:04:03','2021-05-13 00:04:03',NULL,1,5),(1121,'Rerum tenetur sunt quasi et.','Dolores accusamus odit ea et incidunt commodi dolor.','Ratione eaque ea labore occaecati.','2021-05-13 00:04:03','2021-05-13 00:04:03',NULL,1,15);
/*!40000 ALTER TABLE `questionnaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `questionnaire_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (1,'Question 1',1111),(2,'Dolorem quia sequi laborum veritatis exercitationem.',1119),(3,'Cum rerum et magnam et cupiditate.',1116),(4,'Sed quia voluptatum rerum molestiae.',1114),(5,'Et velit voluptatem assumenda dolorum laborum est explicabo.',1118),(6,'Eligendi aliquam veritatis eum odit laborum corporis.',1111),(7,'Exercitationem optio ad excepturi minus aut at.',1111),(8,'Minus dolorum itaque magnam dolorum animi.',1113),(9,'Quam voluptatem eligendi est dolorum consectetur doloribus nisi.',1112),(10,'Numquam sed eius facere voluptate.',1120),(11,'Ullam doloremque et dolores rerum rerum magnam.',1114),(12,'Ad est ipsa enim perferendis vitae voluptatum.',1120),(13,'Sint aperiam beatae nesciunt doloremque et.',1121),(14,'Quaerat quo autem non recusandae iure.',1121),(15,'Sint quis fuga ut.',1118),(16,'Quae eius quam facilis rem.',1119),(17,'Reiciendis suscipit fuga autem.',1117),(18,'Sed minus veniam perspiciatis.',1119),(19,'Laudantium voluptas rerum ducimus aliquam rem.',1117),(20,'Officiis harum hic et ducimus numquam molestiae.',1121),(21,'Id accusantium possimus laborum dolores.',1118),(22,'Velit voluptas culpa accusamus officiis eum.',1111),(23,'Nesciunt sed ea magni.',1111),(24,'Dolor error magnam adipisci et qui et nemo itaque.',1119),(25,'Assumenda corporis illum illum nam non hic.',1114),(26,'Ut et exercitationem qui et inventore.',1112),(27,'Provident quasi et magni fugit.',1120),(28,'In est et veritatis aut.',1121),(29,'Quam quasi aperiam quia consequatur aspernatur nostrum.',1112),(30,'Sit sed beatae tempora dolorem ut blanditiis quos a.',1112),(31,'Error ullam quasi modi sed ea vitae nihil.',1121);
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `results`
--

DROP TABLE IF EXISTS `results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `results` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `response` tinyint(1) NOT NULL,
  `answer_id` int(10) unsigned DEFAULT NULL,
  `completed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `results`
--

LOCK TABLES `results` WRITE;
/*!40000 ALTER TABLE `results` DISABLE KEYS */;
INSERT INTO `results` VALUES (1,4,1,'2021-03-21 00:00:00'),(2,1,32,'2021-05-13 01:04:03'),(3,1,25,'2021-05-13 01:04:03'),(4,1,7,'2021-05-13 01:04:03'),(5,1,28,'2021-05-13 01:04:03'),(6,1,29,'2021-05-13 01:04:03'),(7,1,12,'2021-05-13 01:04:03'),(8,1,28,'2021-05-13 01:04:03'),(9,1,33,'2021-05-13 01:04:03'),(10,1,21,'2021-05-13 01:04:03'),(11,1,12,'2021-05-13 01:04:03'),(12,1,29,'2021-05-13 01:04:03'),(13,1,16,'2021-05-13 01:04:03'),(14,1,33,'2021-05-13 01:04:03'),(15,1,31,'2021-05-13 01:04:03'),(16,1,32,'2021-05-13 01:04:03'),(17,1,14,'2021-05-13 01:04:03'),(18,1,7,'2021-05-13 01:04:03'),(19,1,8,'2021-05-13 01:04:03'),(20,1,19,'2021-05-13 01:04:03'),(21,1,28,'2021-05-13 01:04:03'),(22,1,29,'2021-05-13 01:04:03'),(23,1,20,'2021-05-13 01:04:03'),(24,1,2,'2021-05-13 01:04:03'),(25,1,26,'2021-05-13 01:04:03'),(26,1,11,'2021-05-13 01:04:03'),(27,1,15,'2021-05-13 01:04:03'),(28,1,19,'2021-05-13 01:04:03'),(29,1,2,'2021-05-13 01:04:03'),(30,1,28,'2021-05-13 01:04:03'),(31,1,14,'2021-05-13 01:04:03');
/*!40000 ALTER TABLE `results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1000,1),(1000,6),(1000,8),(1000,10);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1000,'administrator',NULL,NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(10) unsigned DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Kellie','kel@email.com','$2y$10$JaDJ9ESit6eveanRRtvAEOL3gxpPKUM5nUj8iTDnKcWca0.2usOnW',1000,'zNpd6yFUtw',NULL,NULL),(2,'Dr. Kody Stoltenberg PhD','leta.weissnat@mclaughlin.com','$2y$10$wCkYzuSM7Xy3vFWnRLBoauVq775k/SuwApF0V1iXXlM93GvsmW4Ny',1000,'Cwm5miuD5a','2021-05-13 00:04:03','2021-05-13 00:04:03'),(3,'Anya Wunsch','brad02@yahoo.com','$2y$10$VM.gDLhIzpDFKYHN64w/neeqdksOurQzs0w7Q2BaYyyCKWqKO8p1i',1000,'rSYJWBUGHA','2021-05-13 00:04:03','2021-05-13 00:04:03'),(4,'Dr. Casper Willms','mcclure.claire@yahoo.com','$2y$10$mSNGhE3IlUpzadpestOkV.fCiQFN0bNQWcOhooDQEhKGbWWP8HtSu',1000,'D1FiBdnWwl','2021-05-13 00:04:03','2021-05-13 00:04:03'),(5,'Kaylah Mayert','nikko69@willms.biz','$2y$10$Y65ds9iaTP5ShTZPedRKsuXa3/od/GplwNGiuVMs0OHcsRzP/xu5q',1000,'YWn7r8Hpwl','2021-05-13 00:04:03','2021-05-13 00:04:03'),(6,'Mr. Conor Conroy II','daniel.roosevelt@torphy.com','$2y$10$.18N6I/u9fZ1J/dDLyHVUOIZJo/TEEkqHUELhwvTUxxRUI58N6s1y',1000,'txk5KtK2tJ','2021-05-13 00:04:03','2021-05-13 00:04:03'),(7,'Orpha Kautzer','vlesch@cormier.com','$2y$10$Nfi3ZWyXyri.XstBzL9QlOAP.vweswn30HN0gKBvgLL0ux8uxjtVa',1000,'hgPEbuCXyn','2021-05-13 00:04:03','2021-05-13 00:04:03'),(8,'Eleanora Beahan III','will36@hotmail.com','$2y$10$YoUXKtVDmS0lnRcNuUsp1ueQdMX5h1Bjg7dLEnCpSq54kO2XwTX/2',1000,'LNssUMaHuK','2021-05-13 00:04:03','2021-05-13 00:04:03'),(9,'Maegan Pfannerstill IV','ross71@lockman.org','$2y$10$co5Oswggo3OaOWoPWA3HD.25b7DBU4Sz4PY3uc9nTgDs/f/Uq4X0e',1000,'IseNKnTwlA','2021-05-13 00:04:03','2021-05-13 00:04:03'),(10,'Ken Sawayn','econn@gmail.com','$2y$10$HE.2Pbmp5t7qbBc2z4k69eWvkFsLimZyLa/pfxPCKFQ0fRiIz0p42',1000,'UwZVt4eZeb','2021-05-13 00:04:03','2021-05-13 00:04:03'),(11,'Shyann Schumm','cbarrows@mohr.net','$2y$10$zyMrve9rWuZLXZhci.TLjOK7AsNKG17/5A..rzESlueIACNzqfsaK',1000,'SRuCM2OTjG','2021-05-13 00:04:03','2021-05-13 00:04:03'),(12,'Freda Schultz','becker.markus@yahoo.com','$2y$10$XRBB16OYY07vGLJCZsPTsOx5N1zz.khNYB.VpCs9SEj0C4eb.Qax6',1000,'UmOhxWBIkx','2021-05-13 00:04:03','2021-05-13 00:04:03'),(13,'Dr. Jennings Wolf Sr.','julian73@satterfield.com','$2y$10$kQQXqWyoSawBthT/EldjKe9T4Ws//7kMgziS6QmD6kREGDFp2QVBW',1000,'ncQHN9tFsP','2021-05-13 00:04:03','2021-05-13 00:04:03'),(14,'Hannah Kilback','altenwerth.javier@hotmail.com','$2y$10$rG0sSYTpujsKcIwm8WNBKOG6ka/RIuvYBHwJz0ui3dUde8PYN836.',1000,'stKIVybRs3','2021-05-13 00:04:03','2021-05-13 00:04:03'),(15,'Hubert Ortiz','raven94@ortiz.org','$2y$10$cJTC1yIzx.Fphsf76ifZaeUYV3ragPmdqcrLHEFXT1UcTILMeWGmu',1000,'ntiScwZUdU','2021-05-13 00:04:03','2021-05-13 00:04:03'),(16,'Dr. Oma Schumm','stacy48@yahoo.com','$2y$10$9beG1/TbGQLeQeKbq/gTRuXWQIrNuAuZ5exA58QyAZEby5LDrY7Ti',1000,'wdEP8vkNRX','2021-05-13 00:04:03','2021-05-13 00:04:03'),(17,'Anita Kovacek PhD','alek.bartoletti@gmail.com','$2y$10$RHWmlOuOiKeCSFm5zrd6Gud3JR1CS1A2vPFn9HrN4igiHE/dhsXri',1000,'Pjejpb341l','2021-05-13 00:04:03','2021-05-13 00:04:03'),(18,'Mr. Randy Schowalter','jroberts@hotmail.com','$2y$10$52NEIvmj8JrXWMGXZ1s1UOFS.icXw/gg/j7VX67z3NOVHfIsqaoua',1000,'q8XYq0kVFQ','2021-05-13 00:04:03','2021-05-13 00:04:03'),(19,'Ms. Lucienne Blanda PhD','rfeeney@yahoo.com','$2y$10$dyotfHt6paWyTFUkaOm8B.3671dFMrM4I6AKT9b//2JqqrurcN8Rm',1000,'QGOSs9p8ja','2021-05-13 00:04:03','2021-05-13 00:04:03'),(20,'Kamron Vandervort','linnie.pacocha@hotmail.com','$2y$10$9WIxjNhrx4epEsyAd83I9upqCgAoLXBAi4oB2oHNnsOL0f93ROZLi',1000,'JysLpUuJ5s','2021-05-13 00:04:03','2021-05-13 00:04:03'),(21,'Misty Dibbert','ucrist@hamill.info','$2y$10$TjdkaM2unmXtO3rc3xpSoOV2eQ/.3mX.Lij6kh7YoElu8AffgGEci',1000,'ZbIjpv1O84','2021-05-13 00:04:03','2021-05-13 00:04:03'),(22,'Melyssa Stokes','jairo.mann@hotmail.com','$2y$10$n4DnpYEqcK.pF6y0GRZCHeMpVwBkvG/jhBrByjiQtCxrmoutZlS.a',1000,'RgRSiq2u7s','2021-05-13 00:04:03','2021-05-13 00:04:03'),(23,'Elyse Auer PhD','wgleichner@hotmail.com','$2y$10$XF1.o/nJpLrxaVt0AVE11epz3G7cjVmwlv96kW8M0Ztwy1Y/avpke',1000,'8pHbr1pSbV','2021-05-13 00:04:03','2021-05-13 00:04:03'),(24,'Enrico Hauck IV','adella24@lindgren.biz','$2y$10$f1ChZxngdWLwBblmtbOAcee7ggmtS3HZDpjs.acJ2aWvlUI8x30Yy',1000,'jHS3ZocGbN','2021-05-13 00:04:03','2021-05-13 00:04:03'),(25,'Dalton Labadie','jessika62@yahoo.com','$2y$10$4I2u17QH6WafhZ3C0ncoyeMypncQQhl6DXPEjIf6CU0c4VfTwhOq6',1000,'fkfUwcrh8G','2021-05-13 00:04:03','2021-05-13 00:04:03'),(26,'Lolita Wilkinson','sylvan.larson@yahoo.com','$2y$10$HPfIAkleXtWjPpC1GOtVE.2mn6a0t02/JhUuMF5y5QShk384HhaZ6',1000,'v85tWw2g5U','2021-05-13 00:04:03','2021-05-13 00:04:03'),(27,'Mrs. Anahi Smitham','rpowlowski@feil.com','$2y$10$VfQbi7vz5hx148nTczcpNOJULyUiKBfhowhHa6.6zdqVSCV77skDq',1000,'2vWZZIZIof','2021-05-13 00:04:03','2021-05-13 00:04:03'),(28,'Prof. Obie Bergnaum','qhessel@gmail.com','$2y$10$bitxSVXaOs8PzPUC/o3Gv.zCBxGRWgUuHCvK6PSq22HdTc8C/N0S6',1000,'DKOaWKW1t1','2021-05-13 00:04:03','2021-05-13 00:04:03'),(29,'Elisabeth Fritsch','leta.hudson@hotmail.com','$2y$10$qDsWFfwvbi8WekJbyW2/o.I/q55nCdHQ8/2WJJT2RVwHRjgoclKXq',1000,'YD1UI40lwI','2021-05-13 00:04:03','2021-05-13 00:04:03'),(30,'Allison Veum','xstehr@mckenzie.biz','$2y$10$pStFe6H.2XrrHy8Hpl3YPe0xDjeg3xxeu9HMP6tAICPZrIINxUBtm',1000,'LTYtOnGdKU','2021-05-13 00:04:03','2021-05-13 00:04:03'),(31,'Caterina Beahan','mayert.zelda@hotmail.com','$2y$10$NT/ROl/xseenxro0AFLbHut04HUOFa3MZyVKMxRejYeXofb/TZTji',1000,'b8WLmWjZbI','2021-05-13 00:04:03','2021-05-13 00:04:03');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-13  2:22:45
