<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
* Frontend route
*/

Route::group(['prefix' => 'api/v1'], function(){
    Route::resource('questionnaires', 'CompleteController');
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {

Auth::routes();

Route::resource('/home', 'HomeController');

Route::resource('/admin/questionnaire/questionnaires', 'QuestionnaireController');

Route::get('/admin/questionnaire/create', 'QuestionnaireController@create');

Route::get('/questionnaires/{questionnaire}/show', 'QuestionnaireController@show');

Route::get('/questionnaires/{questionnaire}/edit', 'QuestionnaireController@edit');

Route::post('/questionnaires/{questionnaire}/edit', 'QuestionnaireController@update');

Route::get('/questionnaires/{questionnaire}/destroy', 'QuestionnaireController@destroy');

Route::get('/questionnaires/{questionnaire}/questions/create', 'QuestionController@create');

Route::post('/admin/question/create', 'QuestionController@store');

Route::post('/admin/answer/create', 'AnswersController@store');

Route::get('/admin/answer/create/{id}', 'AnswersController@create');

Route::get('/questionnaires/{questionnaire}/results/show', 'ResultsController@show');

});

