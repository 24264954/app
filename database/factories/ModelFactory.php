<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;


$factory->define(App\User::class, function ($faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->email,
        'password' => bcrypt(Str::random(10)),
        'role_id' => App\Role::pluck('id')->random(),
        'remember_token' => $faker->regexify('[A-Za-z0-9]{10}'),
    ];
});


$factory->define(App\Questionnaire::class, function ($faker) {
    return [
        'title' => $faker->sentence,
        'description' => $faker->sentence,
        'ethical_statement' => $faker->sentence,
        'active' => 1,
        'user_id' => App\User::pluck('id')->random(),
    ];
});

$factory->define(App\Question::class, function ($faker) {
    return [
        'question' => $faker->sentence,
        'questionnaire_id' => App\Questionnaire::pluck('id')->random(),
    ];
});

$factory->define(App\Answer::class, function ($faker) {
    return [
        'answer' => $faker->sentence,
        'answer' => $faker->sentence,
        'answer' => $faker->sentence,
        'question_id' => App\Question::pluck('id')->random(),
    ];
});

$factory->define(App\Result::class, function ($faker) {
    return [
        'response' => 1,
        'answer_id' => App\Answer::pluck('id')->random(),
    ];
});