<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // add a known user

        DB::table('users')->insert([
            ['id' => "1",
            'name' => "Kellie",
            'email' => 'kel@email.com',
            'password' => bcrypt('password'),
            'role_id' => '1000',
            'remember_token' => Str::random(10),
            ],
        ]);
    }
}
