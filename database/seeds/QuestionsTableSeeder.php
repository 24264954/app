<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // add a known question

        DB::table('questions')->insert([
            ['id' => 1, 
            'question' => 'Question 1',
            'questionnaire_id' => '1111',
            ],
        ]);
    }
}
