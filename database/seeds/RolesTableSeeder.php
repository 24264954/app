<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // add a known role

        DB::table('roles')->insert([
            ['id' => 1000, 
            'role' => "administrator",
            ],
        ]);
    }
}
