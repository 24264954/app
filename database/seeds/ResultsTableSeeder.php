<?php

use Illuminate\Database\Seeder;

class ResultsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // add a known result

         DB::table('results')->insert([
            ['id' => 1, 
            'response' => "4",
            'answer_id' => '1',
            'completed' => '2021-03-21',
            ],
        ]);
    }
}
