<?php

use Illuminate\Database\Seeder;

class AnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // add a known answer

        DB::table('answers')->insert([
            ['id' => 1, 
            'answer' => 'answer1',
            ],
            ['id' => 2, 
            'answer' => 'answer2',
            ],
            ['id' => 3, 
            'answer' => 'answer3',
            ],
        ]);
    }
}
