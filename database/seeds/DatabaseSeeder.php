<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Role;
use App\User;
use App\Questionnaire;
use App\Question;
use App\Answer;
use App\Result;


class DatabaseSeeder extends Seeder
{
    
    public function run() 
    {
        /*
            //disable foreign key check for this connection before running seeders
          DB::statement('SET FOREIGN_KEY_CHECKS=0;');
          Model::unguard();


          User::truncate();

          //re-enable foreign key check for this connection
          DB::statement('SET FOREIGN_KEY_CHECKS=1;');

            Model::reguard();
        */

        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(QuestionnairesTableSeeder::class);
        $this->call(QuestionsTableSeeder::class);
        $this->call(AnswersTableSeeder::class);
        $this->call(ResultsTableSeeder::class);

        factory(User::class, 30)->create();
        factory(Questionnaire::class, 10)->create();
        factory(Question::class, 30)->create();
        factory(Answer::class, 30)->create();
        factory(Result::class, 30)->create();

    }

}
