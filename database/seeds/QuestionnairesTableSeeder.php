<?php

use Illuminate\Database\Seeder;

class QuestionnairesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // add a known questionnaire

        DB::table('questionnaires')->insert([
            ['id' => 1111, 
            'title' => "Questionnaire 1",
            'description' => 'This is a questionnaire',
            'ethical_statement' => 'This is an ethical statement',
            'active' => 1,
            'user_id' => '1',
            ],
        ]);
    }
}
