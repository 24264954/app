@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                        
                <div class="card-header">You are logged in, {{ Auth::user()->name }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                {{ Form::open(array('action' => 'QuestionnaireController@index', 'method' => 'get')) }}
                <div class="row">
                {!! Form::submit('My Questionnaires', ['class' => 'button']) !!}
                </div>
                {{ Form::close() }}
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
