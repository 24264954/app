@extends('layouts.app')

@section('content')

<h1>Create Question</h1>



{!! Form::open(array('action' => 'QuestionController@store', 'id' => 'createquestion')) !!}
@csrf


    <h3>{{ $questionnaire->title }}</h3>

    <div class="row large-12 columns">
        {!! Form::label('question', 'Question:') !!}
        {!! Form::text('question[question]') !!}
        {!! Form::hidden('question[questionnaire_id]', $questionnaire->id) !!}

    @error('question.question')
        <p class="text-danger">{{ $message }}</p>
    @enderror
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Create Answers', ['class' => 'button']) !!}
    </div>
{!! Form::close() !!}

{{ Form::open(array('action' => 'QuestionnaireController@index', 'method' => 'get')) }}
                <div class="row">
                {!! Form::submit('My Questionnaires', ['class' => 'button']) !!}
                </div>
{{ Form::close() }}

@endsection
