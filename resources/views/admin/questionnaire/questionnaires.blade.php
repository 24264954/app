@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">


                <h1>My Questionnaires</h1>
                
                    <section>
                        @if (isset ($questionnaires))

                                <ul>
                                @foreach ($questionnaires as $questionnaire)
                                    <h3>{{ $questionnaire->title }}</h3>
                                    <input type="checkbox" data-id="{{ $questionnaire->id }}" name="active" class="js-switch" {{ $questionnaire->active == 1 ? 'checked' : '' }}>
                                    <p>

                                        <a class="button" href="/questionnaires/{{ $questionnaire->id }}/questions/create">Add Question</a>
                                        <a class="button" href="/questionnaires/{{ $questionnaire->id }}/show">View Quesitonnaire</a>
                                        <a class="button" href="/questionnaires/{{ $questionnaire->id }}/edit">Edit Questionnaire</a>
                                        <a class="button" onclick="return confirm('Are you sure?')" href="/questionnaires/{{ $questionnaire->id }}/destroy">Delete Questionniare</a>
                                        <a class="button" href="/questionnaires/{{ $questionnaire->id }}/results/show">View Responses</a>
                                        </p>
                                @endforeach
                                </ul>
                                @else
                                    <p> no questionnaires added yet </p>
                                @endif
                                </section>

                                <script>let elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

                                elems.forEach(function(html) {
                                        let switchery = new Switchery(html,  { size: 'small' });
                                });

                                $(document).ready(function(){
                                    $('.js-switch').change(function () {
                                        let active = $(this).prop('checked') === true ? 1 : 0;
                                        let questionnaireId = $(this).data('id');
                                        $.ajax({
                                            type: "GET",
                                            dataType: "json",
                                            url: 'http://127.0.0.1:8000/admin/questionnaire/questionnaires',
                                            data: {'active': active, 'questionnaire_id': questionnaireId},
                                            success: function (data) {
                                                toastr.options.closeButton = true;
                                                toastr.options.closeMethod = 'fadeOut';
                                                toastr.options.closeDuration = 100;
                                                toastr.success(data.message);
                                            }
                                        });
                                    });
                                });
                                </script>

                                

                    {{ Form::open(array('action' => 'QuestionnaireController@create', 'method' => 'get')) }}
                            <div class="row">
                            {!! Form::submit('Create Questionnaire', ['class' => 'button']) !!}
                            </div>
                    {{ Form::close() }}

            </div>
        </div>
    </div >
</div>
                
@endsection
    
