@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <h1>Edit Questionnaire - {{ $questionnaire->title }}</h1>

                <p>Edit Questionnaire details</p>

                {!! Form::model(array('action' => 'QuestionnaireController@update', 'id' => 'createquestionnaire')) !!}
                @csrf
                <div class="row large-12 columns">
                    {!! Form::label('title', 'Title:') !!}
                    {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}

                    @error('title')
                        <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="row large-12 columns">
                    {!! Form::label('description', 'Description:') !!}
                    {!! Form::textarea('description', null, ['class' => 'large-8 columns']) !!}

                    @error('description')
                        <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="row large-12 columns">
                    {!! Form::label('ethical_statement', 'Ethical Statment:') !!}
                    {!! Form::text('ethical_statement', null, ['class' => 'large-8 columns']) !!}
                </div>

                <div class="row large-12 columns">
                    {!! Form::label('active', 'Active:') !!}
                    {!! Form::select('active', array('0' => 'Inactive', '1' => 'Active'), null,['class' => 'large-8 columns', 'multiple']) !!}
                </div> 

                <div class="row large-4 columns">
                    {!! Form::submit('Update Questionnaire', ['class' => 'button']) !!}
                </div>

{!! Form::close() !!}

{{ Form::open(array('action' => 'QuestionnaireController@index', 'method' => 'get')) }}
                <div class="row">
                {!! Form::submit('My Questionnaires', ['class' => 'button']) !!}
                </div>
{{ Form::close() }}


{


@endsection