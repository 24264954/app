@extends('layouts.app')


@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                        <h1>{{ $questionnaire->title }}</h1>

                        <p>{{ $questionnaire->description}} </p>

                        <p>{{ $questionnaire->ethical_statement }} </p>

                        <p>creator: {{ $questionnaire->user_id }}</p>

                        <p>{{ $questionnaire->active }} </p>

                        @foreach($questionnaire->questions as $question)

                            <p>{{ $question->question }} </p>

                                <ul>

                                @foreach($question->answers as $answer)

                                    <li>{{ $answer->answer }} </li>
                                
                                @endforeach

                                </ul>

@endforeach

    


    {{ Form::open(array('action' => 'QuestionnaireController@index', 'method' => 'get')) }}
                <div class="row">
                {!! Form::submit('My Questionnaires', ['class' => 'button']) !!}
                </div>
    {{ Form::close() }}
    

    <a class="button" href="/questionnaires/{{ $questionnaire->id }}/edit">Edit Questionnaire</a>
    
    <a class="button" onclick="return confirm('Are you sure?')" href="/questionnaires/{{ $questionnaire->id }}/destroy">Delete Questionniare</a>
    
@endsection

