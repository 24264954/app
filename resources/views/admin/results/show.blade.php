@extends('layouts.app')

@section('content')

    <h1>{{ $questionnaire->title }}</h1>

    <p id="response">{{ $questionnaire->description}} </p>

    <p id="response">{{ $questionnaire->ethical_statement }} </p>

    <p id="response">creator: {{ $questionnaire->user_id }}</p>

    <p id="response">{{ $questionnaire->active }} </p>

    @foreach($questionnaire->questions as $question)

        <p id="responses">{{ $question->question }} </p>

            <ul>

            @foreach($question->answers as $answer)

                <li id="responses">{{ $answer->answer }} </li>
            
            @endforeach

                @foreach($answer->results as $result)

                    <li id="responses">{{ $result->result }}</li>
                
                @endforeach

            </ul>

    @endforeach

    


    {{ Form::open(array('action' => 'QuestionnaireController@index', 'method' => 'get')) }}
                <div class="row">
                {!! Form::submit('My Questionnaires', ['class' => 'button']) !!}
                </div>
    {{ Form::close() }}
    
    
@endsection

