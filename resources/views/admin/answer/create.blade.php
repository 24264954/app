@extends('layouts.app')


@section('content')

<h1>Create Answers</h1>

{!! Form::open(array('action' => 'AnswersController@store', 'id' => 'createanswers')) !!}
@csrf

    <div class="row large-12 columns">
        {!! Form::label('answer1', 'Answer:') !!}
        {!! Form::text('answers[answer1]') !!}
        {!! Form::hidden('answers[question_id]', $question_id) !!}

        @error('answers.answer1')
            <small class="text-danger">{{ $message }}</small>
        @enderror

    </div>


    <div class="row large-12 columns">
        {!! Form::label('answer2', 'Answer:') !!}
        {!! Form::text('answers[answer2]') !!}

        @error('answers.answer2')
            <small class="text-danger">{{ $message }}</small>
        @enderror
    </div>

    <div class="row large-12 columns">
        {!! Form::label('answer3', 'Answer:') !!}
        {!! Form::text('answers[answer3]') !!}

        @error('answers.answer3')
            <small class="text-danger">{{ $message }}</small>
        @enderror
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Complete', ['class' => 'button', 'name' => 'submitbutton', 'value' => 'complete']) !!}
    </div>


{!! Form::close() !!}



<button action = "QuestionnaireController@index", method = "get", class="btn btn-back">My Questionnaires</button>


@endsection