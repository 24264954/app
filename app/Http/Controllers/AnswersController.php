<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facade\Input;
use App\Questionnaire;
use App\Question;
use App\Answer;
use App\Main;


class AnswersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($question_id)
    {

        return view('admin/answer/create', compact('question_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = request()->validate ([
            'answers.answer1' => 'required',
            'answers.answer2' => 'required',
            'answers.answer3' => 'required',
            'answers.question_id' => 'required', 
        ]);

        $answers = Answer::create($data['answers']);
        $answers->save();

        return redirect()->action('QuestionnaireController@index');

    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $answer = Answer::findOrFail($id);

       return view('/admin/answer/edit')->withAnswer($answer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $answer = Answer::findOrFail($id);

        $this->validate($request, [
            'answer' => 'required', 
        ]);

        $answer = Answer::edit($request->all());
        $answer->save();

        if ($request->has('edit-more')) {
            return view('admin/answer/edit');
        }
        else if ($request->has('complete')) {
            return view('admin/questionnaire/questionnaires');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
