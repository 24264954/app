<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;
use App\User;
use App\Question;
use App\Answer;
use Auth;

class ResultsController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Questionnaire $questionnaire)
    {

        $questionnaire->load('questions.answers.results');
    
        return view('admin/results/show', compact('questionnaire'));

        
    }

}
