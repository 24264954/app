<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        if (Gate::allows('see_all_users')){
   
            $users = User::all();
   
            return view('admin/users/index', ['users' => $users]);
        }
        return view('/home');
   
   
    }
   
}
