<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;

class CompleteController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all the questionnaires for the user
        $questionnaires = Questionnaire::where('user_id', Auth::user()->id)->get();

        //return $questionnaires;
        //return response()->json(['data' => $questionnaires], 200);
        return response()->json([
            'data' => $this->transformCollection($questionnaires)
        ], 200); //transform method and basic secuirty added
    }


    public function show($id)
    {
        // return $questionnaire; 
        $questionnaire = Questionnaire::where('id', $id)->first();

             //if questionnaire does not exist
             if(!$questionnaire) {
                 return response()->json([
                     'error' => ['message' => 'Questionnaire does not exist']
                 ], 404);
             }

             // if questionnaire exists, transfrom it
             return response()->json([
                 'data' => $this->transform($article)
             ], 200);
    }

            /*
        * Separate individual questionnaires to pass through the transform method.
        */
        private function transformCollection($questionnaires){
            return array_map([$this, 'transform'], $questionnaires->toArray());
        }


        /*
        * transform a questionnaire to restrict fields and change column names.
        *
        * 
        */
        private function transform($questionnaires){
            return [
                'questionnaire_id' => $questionnaires['id'],
                'questionnaire_title' => $questionnaires['title'],
                'questionnaire_description' => $questionnaires['description'],
                'questionnaire_ethical_statement' => $questionnaires['ethical_statement'],
            ];
        }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
