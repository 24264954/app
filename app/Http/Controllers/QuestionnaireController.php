<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;
use App\User;
use App\Question;
use App\Answer;
use Auth;

class QuestionnaireController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all the questionnaires for the user
        $questionnaires = Questionnaire::where('user_id', Auth::user()->id)->get();

        return view('/admin/questionnaire/questionnaires')->withQuestionnaires($questionnaires);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/questionnaire/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        {
            $this->validate($request, [
              'title' => 'required', 
              'description' => 'required', 
            ]);

            $questionnaire = Questionnaire::create($request->all());
            $questionnaire->user_id = Auth::user()->id;
            $questionnaire->save();

            return redirect('/admin/questionnaire/questionnaires');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Questionnaire $questionnaire)
    {

        $questionnaire->load('questions.answers');
    
        return view('admin/questionnaire/show', compact('questionnaire'));

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $questionnaire = Questionnaire::findOrFail($id);

        return view('admin/questionnaire/edit')->withQuestionnaire($questionnaire);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $questionnaire = Questionnaire::findOrFail($id);

        $questionnaire->update($request->all());
        $questionnaire->save();

        return redirect('/admin/questionnaire/questionnaires');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questionnaire = Questionnaire::findOrFail($id);

        $questionnaire->delete();

        return redirect('/admin/questionnaire/questionnaires');
    }

    public function updateActive(Request $request)
    {
        $questionnaire = Questionnaire::findOrFail($request->questionnaire_id);
        $questionnaire->active = $request->active;
        $questionnaire->save();
    
        return response()->json(['message' => 'Questionnaire active updated successfully.']);
    }
}
