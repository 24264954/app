<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{

    protected $fillable = [
        'title', 
        'description', 
        'ethical_statement',
        'active'];

    
    public function user() {
        return $this->hasOne('App\User', 'user_id');
    }

    public function questions() {
        return $this->hasMany('App\Question');
    }
}
