<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{

    protected $fillable = [
        'answer',
        'question_id',
    ];
    
    public function question() {
        return $this->belongsTo('App\Question', 'question_id');
    }

    public function results() {
        return $this->hasMany('App\Result');
    }

    public $timestamps = false;
}
