<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
   protected $fillable = [
       'response',
   ];

   public function answers() {
       return $this->belongsTo('App\Answer');
   }

   public $timestamps = false;
}
