<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

    protected $fillable = [
        'question',
        'questionnaire_id',
    ];

    public function questionnaires() {
        return $this->belongsToMany('App\Questionnaire', 'questionnaire_id');
    }

    public function answers() {
        return $this->hasMany('App\Answer');
    }

    public $timestamps = false;

}
